\context Staff = "guia" \with {
	\consists Ambitus_engraver
	%fontSize = #-3
	%\override StaffSymbol #'staff-space = #(magstep -3)
} <<
	\set Staff.instrumentName = "Voz"
	\set Staff.shortInstrumentName = ##f
	\set Staff.midiInstrument = "Voice Oohs"
	\set Score.skipBars = ##t
	\set Staff.printKeyCancellation = ##f
	\new Voice \global
	\new Voice \globalTempo

	\context Voice = "soprano" {
		\override Voice.TextScript #'padding = #2.0
		\override MultiMeasureRest #'expand-limit = 1

		\time 4/4
		\clef "treble"

		\key d \major

		R1*2  |
		r4 a' 8 a' 4 a' a' 8 ~  |
		a' 4 g' 8 fis' 4 g' 8 fis' 4 ~  |
%% 5
		fis' 8 r fis' fis' 4 fis' 8 a' a' ~  |
		a' 8 d' 4. e' 8 fis' e' 4  |
		fis' 2 ( ~ fis' 8 g' fis' e' ~  |
		e' 1 )  |
		r4 a' 8 a' 4 a' a' 8 ~  |
%% 10
		a' 4 g' 8 fis' 4 g' 8 fis' 4 ~  |
		fis' 8 r fis' fis' 4 fis' 8 a' a' ~  |
		a' 8 d' 4. e' 8 fis' e' 4  |
		d' 1  |
		R1  |
%% 15
		r4 d' 8 d' 4 d' e' 8 ~  |
		e' 8 e' 4 e' b 8 b a ~  |
		a 1  |
		R1  |
		r4 d' 8 d' 4 d' g' 8 ~  |
%% 20
		g' 8 g' 4 g' d' d' 8 ~  |
		d' 8 cis' 2.. ~  |
		cis' 2. r4  |
		r4 d' 8 d' 4 d' e' 8 ~  |
		e' 8 e' 4 e' d' 8 e' fis' ~  |
%% 25
		fis' 8 r d' d' d' d' 4 g' 8 ~  |
		g' 8 g' 4 g' d' d' 8 ~  |
		d' 8 cis' 2.. ~  |
		cis' 2. r4  |
		r4 a' 8 a' 4 a' a' 8 ~  |
%% 30
		a' 4 g' 8 fis' 4 g' 8 fis' 4 ~  |
		fis' 8 r fis' fis' 4 fis' 8 a' a' ~  |
		a' 8 d' 4. e' 8 fis' e' 4  |
		fis' 2 ( ~ fis' 8 g' fis' e' ~  |
		e' 1 )  |
%% 35
		r4 a' 8 a' 4 a' a' 8 ~  |
		a' 4 g' 8 fis' 4 g' 8 fis' 4 ~  |
		fis' 8 r fis' fis' 4 fis' 8 a' a' ~  |
		a' 8 d' 4. e' 8 fis' e' 4  |
		d' 1  |
%% 40
		R1  |
		\bar "|."
	}

	\new Lyrics \lyricsto "soprano" {
		Cris -- "to el" Se -- ñor __ re -- su -- ci -- tó, __
		"su a" -- mor es más fuer -- te que la muer -- te. __
		Cris -- "to el" Se -- ñor __ re -- su -- ci -- tó, __
		"su a" -- mor es más fuer -- te que la muer -- te.

		"La I" -- gle -- sia can -- ta "de a" -- le -- grí -- a, __
		los po -- bres sal -- tan de con -- ten -- to, __
		en -- cuen -- tran paz los per -- se -- gui -- dos, __
		con -- sue -- "lo y" per -- dón los pe -- ca -- do -- res. __

		Cris -- "to el" Se -- ñor __ re -- su -- ci -- tó, __
		"su a" -- mor es más fuer -- te que la muer -- te. __
		Cris -- "to el" Se -- ñor __ re -- su -- ci -- tó, __
		"su a" -- mor es más fuer -- te que la muer -- te.
	}
>>
