\context ChordNames
	\chords {
	\set chordChanges = ##t

	% intro
	d1 d1

	% cristo el sennor resucito...
	d1 a1 b1:m g2 a2 d1 a1
	d1 a1 b1:m g2 a2 d1 a1

	% la iglesia canta de alegria...
	d1 a1 g1 g1
	d1 g1 a1 a1
	g1 a1 b1:m g1
	a1 a1

	% cristo el sennor resucito...
	d1 a1 b1:m g2 a2 d1 a1
	d1 a1 b1:m g2 a2 d1
	}
